const env = process.env.SERVER_ENV || 'development';

const config = {
    development: {
        env: 'development',
        writeXmlRequestResponse: true,
        redis: {
            host: '127.0.0.1',
            port: '6379',
            password: ''
        },
        client: {
            port: 4200,
            hostname: 'localhost',
            secure: false
        },
        server: {
            port: 8000,
            hostname: 'localhost',
        },
        database: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'tpc_fce_connect',
            port: 3306
        },
        api: {
            pwdHashSalt: '20fe7a4be6f6a805e1ad5e15a5e9e23285c679f1',
            sendGridApiKey: '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',
            tokenExpiry: 86400,
            refreshTokenExpiry: 86400
        },
        awsS3: {
            accessKeyId: 'AKIAJRVNRA2KDJ5SIAAA',
            secretAccessKey: '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',
            region: 'us-east-1',
            bucket: 'tpconnects-localproperty',
            // orgName: "TPConnects",
            ACL: 'public-read',
            BASE_PATH_AWS: 'http://tpconnects-localproperty.s3.amazonaws.com',
        }
    },
    stagging: {
        env: 'stagging',
        writeXmlRequestResponse: true,
        client: {
            port: process.env.PORT || 8000,
            hostname: process.env.HOSTNAME || 'localhost',
            secure: true
        },
        server: {
            port: process.env.PORT || 8000,
            hostname: process.env.HOSTNAME || 'localhost'
        },
        database: {
            host: 'tpcnvirginia.cjyy1nk8v08l.us-east-1.rds.amazonaws.com',
            user: 'nvirginatpc',
            password: '>n$%y8VH^:B9u}EK',
            database: 'tpc_fce_connect',
            port: 3306,
        },
        redis: {
            host: 'redis-agg-uat.walfn3.0001.use1.cache.amazonaws.com',
            port: '6379',
            password: ''
        },
        api: {
            pwdHashSalt: '20fe7a4be6f6a805e1ad5e15a5e9e23285c679f1',
            sendGridApiKey: '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',
            tokenExpiry: 86400,
            refreshTokenExpiry: 86400
        },


        pkgConfigs: {
            x2joptions: {
                attributeNamePrefix: '@',
                textNodeName: '#text',
                ignoreAttributes: false,
                ignoreNameSpace: true,
                allowBooleanAttributes: false,
                parseNodeValue: true,
                parseAttributeValue: true,
                trimValues: true,
                parseTrueNumberOnly: true,
            },
        },
        pageLimit: 10,
        exportLimit: 1000,
        awsS3: {
            accessKeyId: 'AKIAJRVNRA2KDJ5SIAAA',
            secretAccessKey: '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',
            region: 'us-east-1',
            bucket: 'tpconnects-localproperty',
            // orgName: "TPConnects",
            ACL: 'public-read',
            BASE_PATH_AWS: 'http://tpconnects-localproperty.s3.amazonaws.com',
        },
    },
    production: {
        env: 'production',
        writeXmlRequestResponse: true,
        client: {
            port: process.env.PORT || 3200,
            hostname: process.env.HOSTNAME || 'localhost',
            secure: true
        },
        server: {
            port: process.env.PORT || 3200,
            hostname: process.env.HOSTNAME || 'localhost',
        },
        redis: {
            host: 'redis-agg-uat.walfn3.0001.use1.cache.amazonaws.com',
            port: '6379',
            password: ''
        },
        database: {
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_NAME,
            port: process.env.DB_PORT,
            dialect: process.env.DB_DIALECT,
        },
        api: {
            flightConnectServiceURL: 'http://gds.tpconnects.com/GateWayV10/services/FlightConnectService',
            carConnectServiceURL: 'http://merchandising.tpconnects.com/TpConnectsV7/wsdl/CarsConnect.wsdl',
            activityConnectServiceURL: 'http://merchandising.tpconnects.com/TpConnectsLatestV7/services/ActivityConnect?wsdl',
            hotelConnectServiceURL: 'http://prd.tpconnects.com/TpConnectsV7/services/HotelsConnect?wsdl',
            apiSecretKeyRefreshToken: '___09XXV8237ehdgjhwjgIOSUOI271eyXCCXVVSB8179AKJSJD089D80A09DA--',
            pwdHashSalt: '20fe7a4be6f6a805e1ad5e15a5e9e23285c679f1',
            sendGridApiKey: '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',
            tokenExpiry: '1h',
            refreshTokenExpiry: 86400,
        },
        pkgConfigs: {
            x2joptions: {
                attributeNamePrefix: '@',
                textNodeName: '#text',
                ignoreAttributes: false,
                ignoreNameSpace: true,
                allowBooleanAttributes: false,
                parseNodeValue: true,
                parseAttributeValue: true,
                trimValues: true,
                parseTrueNumberOnly: true,
            },
        },
        pageLimit: 10,
        exportLimit: 1000,
        awsS3: {
            accessKeyId: 'AKIAJRVNRA2KDJ5SIAAA',
            secretAccessKey: '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',
            region: 'us-east-1',
            bucket: 'tpconnects-localproperty',
            // orgName: "TPConnects",
            ACL: 'public-read',
        },
    }
};

config[env].isDev = env === 'development';
config[env].isTest = env === 'stagging';
config[env].isProd = env === 'production';
module.exports = config[env];
