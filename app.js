// const compression = require('compression')
const express = require('express')
require('express-async-errors');
//const uuid = require('uuid/v4')
const session = require('express-session')
//const FileStore = require('session-file-store')(session);
var MemoryStore = require('memorystore')(session)
const bodyParser = require('body-parser');

const path = require('path');
const favicon = require('serve-favicon');
//const logger = require('morgan');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const multitenancy = require('./multitenancy');

//const debug = require('debug')('myapp:app');

const swaggerJSDoc = require('swagger-jsdoc');

//const validateRoutePermission = require('./app/middlewares/permissions.middleware')

// validation rules
global.Validator = require('validatorjs');
global.validRules = require('./validation_rules.js').rules;
global.FUNC = require('./_helpers/functions.js');

// config
const config = require('./config/configJs');

// database config
const db = require('./config/db');

const { httpLogger } = require('./app/helper/logger');
const { logger } = require('./app/utils');
const app = express();
app.use(httpLogger);
app.use(multitenancy);
// app.use(compression({ threshold: 0 }))

var corsOptions = {
  origin: `http` + (config.client.secure ? `s` : ``) + `://` + config.client.hostname + `:` + config.client.port,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  allowedHeaders: ['Authorization', 'X-PINGOTHE', 'Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Custom-header', 'Set-Cookie'],
  exposeHeaders: ['Content-Range', 'X-Content-Range', 'Set-Cookie'],
  credentials: true,
  preflightContinue: false
  // If preflightContinue is set to true, it will remove the authorization from the request, and all other modification from the user
}

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

// let sessionExpiryTime = 5 * 1000//(86400 * 1000 / 24)
// app.use(session({
//   resave: true,
//   saveUninitialized: true,
//   secret: "MySecretKeyToBeChangedInProdWhenDeploy",
//   store: new MemoryStore(),
//   maxAge: new Date(Date.now() + sessionExpiryTime),
//   expires: new Date(Date.now() + sessionExpiryTime)
// }));

let sessionExpiryTime = 60 * 20 * 1000 // 20 minitus
app.use(session({
  key: 'user_sid',
  resave: true,
  saveUninitialized: false,
  secret: "MySecretKeyToBeChangedInProdWhenDeploy",
  store: new MemoryStore(),
  maxAge: sessionExpiryTime,
  expires: sessionExpiryTime,
  rolling: true,
  cookie: { expires: sessionExpiryTime, maxAge: sessionExpiryTime, httpOnly: false }
}));

// app.use((req, res, next) => {
//   console.log(req.session.auth)
//   const path = req.path
//   var checkPath = path.includes("/api/");
//   if(checkPath){
//     if(path == "/api/sign-in" || path == "/api/verify-otp"){
//       next()
//     }else if(!res.cookie.user_sid && req.session.auth){
//       next()
//     }else{
//       req.session.destroy();
//       res.clearCookie('user_sid')
//       res.json({
//         auth:false,
//         data: "Session expire"
//       })
//     }
//   }else{
//     next()
//   }
// })

// app.use(session({
//   genid: (req) => {
//     // console.log('');
//     // console.log('╔═════════════════════════════════════════════════════════════════════════════════╗')
//     // console.log('║ Inside session middleware, genid function                                       ║')
//     // console.log(`║ Request object sessionID from client: ${req.sessionID}`)
//     // console.log('╚═════════════════════════════════════════════════════════════════════════════════╝')
//     // console.log('');
//     return uuid() // use UUIDs for session IDs
//   },
//   store: new FileStore(),
//   secret: 'MySecretKeyToBeChangedInProdWhenDeploy',
//   resave: false,
//   saveUninitialized: true
// }))

// view engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'pug');

// COOKIE CONFIGS
var COOKIE_CONFIG = {
  setHeaders: function (res, path, stat) {
    res.set('Set-Cookie', cookie.serialize('aggregator-cookie', 'test', {
      httpOnly: true,
      maxAge: 60 * 60 * 24 * 7 // 1 week
    }))
  }
}

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', true);
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, x-auth");
//   const err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // error handler
// app.use((err, req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', true);
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, x-auth");
//   // set locals, only providing error in development
//   res.locals.message = err.message; // eslint-disable-line no-param-reassign
//   res.locals.error = config.isDev ? err : {}; // eslint-disable-line no-param-reassign
//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


// Swagger definition
const swaggerDefinition = {
  definition: {
    swagger: '2.0',
    info: {
      title: 'TPConnects Aggregator Platform',
      version: '2.1',
    },
    host: config.server.hostname + `:` + config.server.port,
    basePath: '',
    schemes: [`http` + (config.server.secure ? `s` : ``)],
    consumes: ['application/json'],
    produces: ['application/json'],
    securityDefinitions: {
      authentication: {
        description: 'JWT Authorization header using the Bearer scheme. Example: "Authorization: {token}"',
        type: 'apiKey',
        name: 'Authorization',
        in: 'header',
      },
    },
    security: [{
      authentication: []
    }],
  },
  apis: [
    './app/controllers/users/user.routes.js',
  ]
};


// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(swaggerDefinition);
app.use('/api/swagger/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});


app.use('/api/api-docs', express.static('api-docs'));
// app.use(validateRoutePermission());


require('./app/routes/routes')(app, COOKIE_CONFIG);
//app.options('/api/*');
app.use(express.static(`${__dirname}/public/aggregator-platform`));
//app.use(logger(config.isProd ? 'combined' : 'dev'));
app.use(cookieParser());
// app.use(favicon(path.join(__dirname, 'public', 'favicon/favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));


// bootstrap routes

// require('./app/controllers/Upgrade').versionCheck();
require('./app/routes/routes')(app, '/api');
const json2xls = require('json2xls');

app.use(json2xls.middleware);

// app.get('/*', (req, res) => {
//   res.sendFile(path.join(`${__dirname}/public/aggregator-platform/index.html`));
// });


const appAllRoutes = [];
app._router.stack.forEach(middleware => {
  if (middleware.route) {
    appAllRoutes.push(`${Object.keys(middleware.route.methods)} -> ${middleware.route.path}`);
  }
});

// console.log(appAllRoutes);
// // console.log(JSON.stringify(appAllRoutes, null, 2));

app.listen(config.server.port, config.server.hostname, () => {
  logger.info(`App listening on ${config.server.hostname} port: ${config.server.port}`, '');
  app.emit('appStarted');
});

/*const https = require('https');
const fs = require('fs');
const port = 443;
//var key = fs.readFileSync('/opt/lampp/etc/ssl.crt/x/main.key');
//var cert = fs.readFileSync('/opt/lampp/etc/ssl.crt/x/main.crt');
var key = fs.readFileSync(__dirname + '/ssl/selfsigned.key');
var cert = fs.readFileSync(__dirname + '/ssl/selfsigned.crt');
var options = {
  key: key,
  cert: cert
};
var server = https.createServer(options, app);

server.listen(port, () => {
  console.log("server starting on port : " + port)
});*/

module.exports = app; 
