module.exports = (sequelize, type) => {
    let M_Product_CategoryModel = sequelize.define('M_Product_Category', { 
    cat_id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    Description: type.STRING,
    Updated_By: type.STRING,
    IsActive: type.STRING,
    Last_Updated_Date: type.STRING, 
}, {
    freezeTableName: true,
    timestamps: false
});
 return M_Product_CategoryModel;
};
