module.exports = (sequelize, type) => {
    var userModel = sequelize.define('users', {
        users_id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        email: type.STRING,
        password: type.STRING,
        username: type.STRING,
        rememberme_token: type.STRING,
        profile_image: type.STRING,
        locale: {
            type: type.STRING,
            defaultValue: 'en'
        },
        first_name: type.STRING,
        last_name: type.STRING,
        sex: type.STRING,
        mobile: type.STRING,
        country_id: type.INTEGER,
        status: type.BOOLEAN,
        address_1: type.STRING,
        address_2: type.STRING,
        is_active: type.INTEGER,
        salute: {
            type: type.ENUM,
            values: ['Mr','Master','Mrs.','Miss','Ms.','Dr.']
        },
        role_id: type.INTEGER,
        createdAt: type.DATE,
    }, {
        freezeTableName: true,
        timestamps: false
    });
    userModel.removeAttribute('id');
    return userModel;
};
