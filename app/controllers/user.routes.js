const userController = require('./user.controller');
const userMiddleware = require('./user.middleware');
module.exports = function (app, base) {
     app.post(`${base}/sign-in`, userMiddleware.userValidations, userController.signIn);
}