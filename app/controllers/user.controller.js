const utils_response = require('../utils/utils_response');
const { Op } = require('../../config/db');
const jwt = require('jsonwebtoken');
const config = require('../../config/configJs');

let signIn = async (req, res, next) => {
    let usersModel = req.getInstance('user');
    usersModel.findOne({
            where: { [Op.or]: [{ username }, { email: username }] },
        }).then((user) => {
            if (user) { 
                res.cookie('rememberme', '0', { expires: false, httpOnly: false }); 
                if (req.body.rememberMe) {
                    const token = jwt.sign({ user: user }, config.api.apiSecretKey, { expiresIn: config.api.tokenExpiry });
                    res.cookie('rememberme', '1', { expires: new Date(Date.now() + 3.154e+10), httpOnly: false }); // expire in one year
                    res.cookie('remembermeToken', token, { expires: new Date(Date.now() + 3.154e+10), httpOnly: false }); // expire in one year
                  }
                  const token = jwt.sign({ user: user }, config.api.apiSecretKey, { expiresIn: config.api.tokenExpiry });
                  const refreshToken = jwt.sign({ user: user }, config.api.apiSecretKeyRefreshToken, { expiresIn: config.api.refreshTokenExpiry });
                  setTokenList({ [refreshToken]: { token, refreshToken } });
                  return utils_logs.makeResponse(res, true, 200, 'success', 'Login Successful', 'Successfully Authenticated.', result);
               
            }
            else {
                return utils_response.makeResponse(res, false, 200, 'error', 'Login not successful', 'Username / Email not found');
            }
    
        }).catch((error) => {
            console.log(error);
            res.json({
                type: 'error',
                status: 400,
                msg: 'Oops! Something went wrong while processing your request.',
            });
        });
    }
module.exports = { 
    signIn
} 