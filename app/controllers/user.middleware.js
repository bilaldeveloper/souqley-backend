const Joi = require('@hapi/joi');
const { makeResponse } = require('../utils/utils_response');

const UserSchema = Joi.object({

    email: Joi.string().required().email(),
    password: Joi.string().required(),
     

});
let userValidations = (req, res, next) => {
    let formObj = req.body;
    const { error } = UserSchema.validate(req.body);
    if (error) {
        return makeResponse(res, false, 400, 'danger', 'Validation Error!', (error.details[0].message).replace(/[|&;$%@"<>()+,]/g, ""));
    } else {

        return next();
        // }
    }
}
module.exports = {
    userValidations
}