const makeResponse = (res, apiStatus, resStatus, type, title, message, result = []) => {
    if (type == 'error') {
      errorObj = {
        '#text': '',
        Code: '',
        ShortText: '',
      };
      if (typeof result.Errors === 'object') {
        errorObj['#text'] = result.Errors.Error['#text'];
        errorObj.Code = result.Errors.Error.Code;
        errorObj.ShortText = result.Errors.Error.ShortText;
      } else if (result.Errors == 'string') {
        errorObj['#text'] = result.Errors;
      } else {
        errorObj['#text'] = message;
      }
      res.status(resStatus).json({
        status: apiStatus,
        code: resStatus || 200,
        type: type || 'success',
        title: title || '',
        message: message || '',
        Errors: { Error: errorObj },
      }).end();
    } else {
      res.status(resStatus).json({
        status: apiStatus,
        code: resStatus || 200,
        type: type || 'success',
        title: title || '',
        message: message || '',
        result,
      }).end();
    }
  };
  module.exports = {
    makeResponse,
  };
  