const glob = require('glob');
// const path = require('../');
const winston = require('winston');

module.exports = (app, base) => {
    var path = require('path');
    path = path.join(__dirname, '../');
    console.log('path ...', path);
    let routePath = path + '/controllers/**/*.routes.js';
    glob.sync(routePath).forEach(function (file) {
        require(file)(app, base);
    });
};