module.exports = {
    up: (queryInterface, type) => {
        return queryInterface.createTable('corporate_staff_travellers', {
            traveller_profile_id: {
                type: type.STRING(30),
                primaryKey: true
            },
            organisation_id: {
                type: type.STRING(20),
                allowNull: true
            },
            salutation: {
                type: type.ENUM('MR', 'Mrs', 'Miss', 'Dr'),
                allowNull: false
            },
            first_name: {
                type: type.STRING(50),
                allowNull: false
            },
            last_name: {
                type: type.STRING(50),
                allowNull: false
            },
            middle_name: {
                type: type.STRING(50),
                allowNull: true
            },
            full_name: {
                type: type.STRING(50),
                allowNull: true
            },
            email: {
                type: type.STRING(50),
                allowNull: true
            },
            dob: {
                type: type.STRING(50),
                allowNull: true
            },
            gender: {
                type: type.CHAR(5),
                allowNull: true
            },
            fax: {
                type: type.STRING(50),
                allowNull: true
            },
            status: {
                type: type.STRING(10),
                allowNull: false
            },
            passenger_type: {
                type: type.STRING(50),
                allowNull: true
            },
            company_name: {
                type: type.STRING(50),
                allowNull: true
            },
            job_title: {
                type: type.STRING(255),
                allowNull: true
            },
            division: {
                type: type.STRING(50),
                allowNull: true
            },
            mobile_number: {
                type: type.INTEGER(50),
                allowNull: true
            },
            emergency_contact_no: {
                type: type.INTEGER(50),
                allowNull: true
            },
            emergency_contact_name: {
                type: type.INTEGER(50),
                allowNull: true
            },
            emergency_contact_relation: {
                type: type.STRING(50),
                allowNull: true
            },
            identity_doc_type: {
                type: type.STRING(50),
                allowNull: true
            },
            identity_doc_Id: {
                type: type.STRING(50),
                allowNull: true
            },
            identity_doc_issueDate: {
                type: type.DATE,
                allowNull: true
            },
            identity_doc_expiryDate: {
                type: type.DATE,
                allowNull: true
            },
            identity_doc_placeofissue: {
                type: type.STRING(50),
                allowNull: true
            },
            corporate_name: {
                type: type.STRING(50),
                allowNull: true
            },
            designation: {
                type: type.STRING(50),
                allowNull: true
            },
            employeeId: {
                type: type.STRING(50),
                allowNull: true
            },
            visaId: {
                type: type.INTEGER(50),
                allowNull: true
            },
            vise_type_code: {
                type: type.STRING(10),
                allowNull: true
            },
            visa_EnterBeforeDate: {
                type: type.STRING(50),
                allowNull: true
            },
            visa_EntryQty: {
                type: type.STRING(50),
                allowNull: true
            },
            visa_HostCountryCode: {
                type: type.STRING(50),
                allowNull: true
            },
            visa_StayDuration: {
                type: type.STRING(50),
                allowNull: true
            },
            company_number: {
                type: type.STRING(50),
                allowNull: true
            },
            flightPreferences: {
                type: type.STRING(50),
                allowNull: true
            },
            meal_type: {
                type: type.STRING(50),
                allowNull: true
            },
            seat_preference: {
                type: type.ENUM('W', 'A', 'L', 'C', 'U', 'D', 'M'),
                allowNull: true
            },

            smoking_preference: {
                type: type.BOOLEAN,
                allowNull: true
            },
            hotel_preference: {
                type: type.STRING(50),
                allowNull: true
            },
            car_preference: {
                type: type.STRING(50),
                allowNull: true
            },

            created: {
                type: type.DATE,
                get() {
                    return moment(this.getDataValue('created')).format('DD/MM/YYYY h:mm:ss');
                }
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('test_table');
    }
};